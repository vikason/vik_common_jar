
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import vik.com.StringUtil;
import vik.log.VikLogManager;
import vik.file.FileHandler;
import vik.log.TestLogger;
import vik.mail.TestMailAPI;
import vik.sql.DbConnecter;
import vik.sql.ConnectionFactory;
import vik.sql.SQLBindVariable;
import vik.sql.TestDB;
import vik.sql.db.DerbyConnecter;
import vik.sql.db.MySqlConecter;
import vik.sql.exception.BadSqlFormatException;

import vik.xml.ReadXML;
import vik.xml.XMLReader;
import vik.xml.com.XMLDataBean;
import vik.xml.com.XMLHandler;
import vik.xml.handler.GeneralHandler;
//import vik.log.LoggingExample;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @Desc:Java Class
 * @author : Vikas Kumar Verma
 */
public class Test {

    private static Logger log;

    static {
        try {
            log = TestLogger.getLogger(Test.class.getName());
            log.setLevel(Level.ALL);
        } catch(SecurityException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param a
     */
    public static void main(String a[]) {
        try {
//            System.out.println(StringUtil.getDate("dd/MM/yyyy"));
//            testStringMD5("vikas");
//            testBindSql();
//            removeSapces();
//            caseConverter("jai ShRi rAm");
//            getOsName();
//            getLogger();
//            readXmlData();
//            useXMLReader();
//            testLogger2();
//            testMysqlConnection();
//            testDerbyConnection();
//            testPreparedStatement();
//            bindsql2();
//            dbConnectionTest();
//            readxml();
            sendMail();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     */
    public static void testDerbyConnection() {
        DbConnecter con = new DerbyConnecter(
                "credential",
                "credential",
                "credential",
                true
        );

        ConnectionFactory db = new ConnectionFactory(con);
        Connection conn = null;

        try {
            conn = db.getConnection();
        } catch(ClassNotFoundException | SQLException ex) {
            log.log(Level.SEVERE, null, ex);
        }

        if( conn == null ) {
            log.severe("CONNECTION IS NULL");
        } else {
            log.fine("connection successful");
        }
    }

    /**
     *
     * @param astrValue
     */
    public static void testStringMD5(String astrValue) {
        System.out.println("MD5 Value: " + StringUtil.stringMD5(astrValue));
    }

    /**
     *
     */
    public static void testBindSql() {
        Map map = new HashMap();
        Map parameter = new HashMap();

        parameter.put("1", "jsr");
        parameter.put("2", "siya");
        parameter.put("3", "shiv");
        parameter.put("4", "parvati");

        map.put("sql", "select * from dual where a = ? and b = ? and c = ? and d = ?");
        map.put("parameter", parameter);

        System.out.println("result");
    } // testBindSql

    /**
     *
     */
    public static void removeSapces() {

        StringUtil obj = new StringUtil();

//        System.out.println(obj.removeExtSpace("select           *      from dual"));
        log.fine(obj.removeExtSpace("select           *      from dual"));
    } // removeSapces

    /**
     *
     * @param str
     */
    public static void caseConverter(String str) {
        System.out.println(StringUtil.caseConverter(str));
    }

    /**
     * get current OS name
     */
    public static void getOsName() {
        System.out.println(FileHandler.getOsName());

    }

    /**
     *
     */
    public static void testLogger() {
        try {
            VikLogManager logmanager = new VikLogManager();

            Logger log = logmanager.getLogger("My Test Logger", true);
            // log.setLevel(Level.ALL);
            log.fine("this is test fine message");
            log.finer("this is test finer message");
            log.finest("this is test finest message");
            log.info("this is test info message");
            log.severe("this is test error msg2");
        } catch(Exception ex) {
            log.log(Level.SEVERE, null, ex);
        }

    }

    /**
     *
     */
    public static void readXmlData() {
        Map map = null;
        try {
            map = ReadXML.readMailConfig("D:\\rnd\\SunSeal\\web\\WEB-INF\\project_config.xml");
        } catch(ParserConfigurationException ex) {
            log.log(Level.SEVERE, null, ex);
        } catch(SAXException ex) {
            log.log(Level.SEVERE, null, ex);
        } catch(IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }

        System.out.println(map);
    }

    /**
     *
     */
    public static void useXMLReader() {
//        XMLHandler handler = new MailConfigXMLHandler();
//        XMLHandler handler = new MailConfigXMLHandler();
        XMLHandler handler = new GeneralHandler();

        String filePath = "D:\\rnd\\MamApi\\src\\vik\\xml\\Test.xml";

        XMLReader reader = new XMLReader(filePath, handler);

        // System.out.println("reading first file");
        System.out.println(filePath);
        Map map = null;
        try {
            map = reader.readXMLData();
        } catch(ParserConfigurationException | SAXException | IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }

        System.out.println(map);
    }

    private static void testLogger2() {
        try {
            Logger log = TestLogger.getLogger(Test.class.getName());
            log.info("this is info message after logging ");
            log.severe("this is error message");
        } catch(Exception ex) {
            log.log(Level.SEVERE, null, ex);
        }

    }

    /**
     *
     */
    public static void testMysqlConnection() {
        DbConnecter con = new MySqlConecter(
                "127.0.0.1",
                "3306",
                "JSR",
                "root",
                "vikas"
        );
        ConnectionFactory db = new ConnectionFactory(con);
        Connection conn = null;
        try {
            conn = db.getConnection();
        } catch(ClassNotFoundException | SQLException ex) {
            log.log(Level.SEVERE, null, ex);
        }

        if( conn == null ) {
            log.severe("CONNECTION IS NULL");
        } else {
            log.fine("connection successful");
        }
    }

//    public static void testPreparedStatement() {
//        DbConnecter con = new MySqlConecter(
//                "127.0.0.1",
//                "3306",
//                "JSR",
//                "root",
//                "vikas"
//        );
//        DbUtility db = new DbUtility(con);
//        try {
//            PreparedStatement ps = db.getPreparedStatement("select * from dual");
//
//        } catch(ClassNotFoundException | SQLException ex) {
//            log.log(Level.SEVERE, null, ex);
//        }

    /**
     *
     */
    public static void bindsql2() {
        List list = new ArrayList();
        list.add("vikas");
        list.add("mamta");
        list.add("786");
        String str = "select ?, ?, ? from dual";
        try {
            System.out.println(SQLBindVariable.bindSql(str, list));
        } catch(BadSqlFormatException ex) {
            log.log(Level.SEVERE, null, ex);
        }

    }

    private static void dbConnectionTest() {
        TestDB db = new TestDB();
        db.dbConnectionTest();
    }

    private static void readxml() {
        GeneralHandler gh = new GeneralHandler();
        gh.setIsRemoveSpace(true);

        String str = "D:\\rnd\\MamApi\\src\\vik\\xml\\Test.xml";
        XMLReader xml = new XMLReader(str, gh);
        Map map = null;
        try {
            map = xml.readXMLData();
        } catch(ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }

        List lst = (List) map.get("email");
        XMLDataBean bean;

        for( Object o : lst ) {
            bean = (XMLDataBean) o;
            System.out.println( bean.getTag() + "       " + bean.getValue());

        }



    }

    private static void sendMail() {
        TestMailAPI tma = new TestMailAPI();
        tma.sendMail();
    }
}
/**
 * Expression end is undefined on line 19, column 4 in
 */
