
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vik.com;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.logging.Logger;
import vik.common.Constants;

/**
 *
 * @Desc:Java Class
 * @author : Vikas Kumar Verma
 */
public class StringUtil {

    private Logger log;

    /**
     *
     */
    public static String DATE_FORMAT = "dd/MM/yyyy";
    private static String MD5 = "MD5";

    /**
     * Default Constructor
     */
    public StringUtil() {
    }

    /**
     * Convert String in date format. The default format is "dd/MM/yyyy" to
     * change the default input string format change the value of DATE_FORMAT
     * variable.
     * <p>
     * @param dateString: input date String.
     * <p>
     * @return <code>Date</code>
     * @throws java.text.ParseException
     */
    public Date convertStringDate(String dateString)
            throws ParseException {

        try {
            DateFormat df;
            df = new SimpleDateFormat(DATE_FORMAT);

            return df.parse(dateString);
        } finally {

        }
    }

    /**
     * <p>
     * Return currant System Date in the specified pattern, the pattern should
     * be same as patterned allowed in <code>SompleDateFormat</code> class.</p>
     * <p>
     * @param pattern Pattern in which date required.
     * <p>
     * @return Current Date.
     */
    public static String getDate(String pattern) {
        Date date = Calendar.getInstance().getTime();
        return new SimpleDateFormat(pattern).format(date);
    } // getDate

    /**
     * Convert date in String format. The default format is "dd/MM/yyyy" to
     * change the default input string format change the value of DATE_FORMAT
     * variable.
     * <p>
     * @param date
     * @return String
     */
    public static String convertDateString(Date date) {
        SimpleDateFormat simpleDateFormat
                = new SimpleDateFormat(DATE_FORMAT);
        return simpleDateFormat.format(date);
    }

    /**
     * Convert normal string into MD5 encoded string.
     * <p>
     * @param encodedString <p>
     * @return MD5 encoded string.
     */
    public static String stringMD5(String encodedString) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest
                    .getInstance(MD5);
            byte[] array = md.digest(encodedString.getBytes());
            StringBuilder sb = new StringBuilder();
            for ( int i = 0; i < array.length; ++i ) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
                        .substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    /**
     * Convert string into word case example "Jai Shri Ram"
     * <p>
     * @param str
     * @return String
     */
    public static String caseConverter(String str) {

        str = removeExtSpace(str);

        StringBuilder sb = new StringBuilder();
        char ch[] = str.toCharArray();
        int j = 0;
        for ( int i = 0; i < ch.length; i++ ) {

            if ( ch[i] == ' ' ) {
                j = i + 1;
            }
            if ( (i == 0) || j == i ) {
                char cha = Character.toUpperCase(ch[i]);
                sb.append(Character.toString(cha));
                j = 0;
            } else {
                char cha = Character.toLowerCase(ch[i]);
                sb.append(Character.toString(cha));
            }
        }
        return sb.toString().trim();
    }

    /**
     * Remove extra spaces from the input String.
     * <p>
     * @param str <p>
     * @return
     */
    public static String removeExtSpace(String str) {

        char ch[] = str.toCharArray();
        StringBuilder sb = new StringBuilder();

        int j = 0;

        for ( int i = 0; i < ch.length; i++ ) {
            if ( ch[i] == ' ' ) {
                j++;

            } else {
                if ( j > 0 ) {
                    sb.append(' ');
                    j = 0;
                }
                sb.append(ch[i]);
            }

        }
        return sb.toString().trim();
    }

    /**
     * Deprecated method user rendomString() instead Generate random password
     * string
     * <p>
     * @return password string.
     */
    @Deprecated
    public static String getRandamString() {
        final int PASSWORD_LENGTH = 8;
        StringBuilder sb = new StringBuilder();
        for ( int x = 0; x < PASSWORD_LENGTH; x++ ) {
            sb.append((char) ((int) (Math.random() * 26) + 97));
        }
        Random randomGenerator = new Random();
        sb.append(String.valueOf(randomGenerator.nextInt(1000)));
        return sb.toString();
    }

    /**
     * Returns file name from the path
     * <p>
     * @param path: File Path
     * <p>
     * @return <code>String</code> exact file name
     */
    public static String getFileNameFromPath(String path) {
        int l;
        path = path.replace("\\", "~");
        while ( true ) {
            l = path.indexOf("~");
            path = path.substring(l + 1, path.length());
            if ( l < 0 ) {
                break;
            }
        }

        return path;
    }

    /**
     * Generate random string used to generate password.
     * <p>
     * @return <code>String</code> generated string of 10 character
     */
    public static String randomString() {
        return randomString(10);
    }

    /**
     * Generate random string used to generate password.
     * <p>
     * @param length length of output string
     * <p>
     * @return <code>String</code> generated string
     */
    public static String randomString(int length) {
        String str1 = "thequickbrownfoxjumpsoverthelazydog"
                + "0123456789THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG";
        StringBuilder sb = new StringBuilder(length);
        Random r = new Random();

        for ( int j = 0; j < length; j++ ) {
            sb.append(str1.charAt(r.nextInt(str1.length())));
        }
        return sb.toString();

    } // randomString

    /**
     * Return current time stamp in form YYYYMMDDHH24MISS
     * <p>
     * @return Current Time
     */
    public static String getCurrentTimeStamp() {
        return getCurrentTimeStamp(Constants.DATE_FORMAT);

    } // getCurrentTimeStamp

    /**
     * Return current time stamp in form YYYYMMDDHH24MISS
     * <p>
     * @param format <p>
     * @return
     */
    public static String getCurrentTimeStamp(String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date date = Calendar.getInstance().getTime();
        return simpleDateFormat.format(date);
    }

} // end of class
