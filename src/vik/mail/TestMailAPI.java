package vik.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import vik.common.Constants;
import vik.log.TestLogger;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class TestMailAPI {

    private Logger log = TestLogger.getLogger(TestMailAPI.class.getName());

    public void sendMail() {
        Properties prop;

        prop = System.getProperties();

        prop.setProperty(Constants.USER_NAME_KEY, "vikas.on.app@gmail.com");
        prop.setProperty(Constants.PASSWORD_KEY, "raghunandan123");
        prop.setProperty(Constants.HOST_KEY, "smtp.gmail.com");
        prop.setProperty(Constants.PORT_KEY, "587");
        prop.setProperty(Constants.AUTHORIZATION_KEY, "true");

        List to = new ArrayList();
        to.add("vikas.on@gmail.com");
        MailFactory mf = new MailFactory(prop);
        mf.setSubject("THis is my subject,");
        mf.setBody("This is body");

//        mf.setAttachementFileName("D:\\rnd\\SunSeal\\web\\WEB-INF\\project_config.xml");
        mf.setBcc(to);

        try {
            mf.sendMail();
        } catch(Exception ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }
}
