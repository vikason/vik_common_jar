/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vik.mail;

import java.util.*;
import java.util.logging.Logger;
import javax.activation.*;
import javax.mail.*;
import javax.mail.internet.*;
import vik.com.StringUtil;
import vik.common.Constants;

/**
 *
 * @Desc: Java Class
 * @author : Vikas Kumar Verma
 */
@Deprecated
public class MailUtility implements Constants {

    private static final Logger LOG =
            Logger.getLogger(MailUtility.class.getName());

    /**
     *
     */
    public MailUtility() {
    }


    /**
     * send mail function.
     *
     * @param mapConfig configuration details
     * @param subject mail subjects
     * @param bodyMessage mail body
     * @throws Exception
     */
    public static void sendMail(Map mapConfig, String subject,
            String bodyMessage) throws Exception {
        sendMail(mapConfig, subject, bodyMessage, null);

    }

    /**
     * send mail function.
     *
     * @param mapConfig configuration details
     * @param subject mail subjects
     * @param bodyMessage mail body
     * @param astrfileName attachment file name.
     * @throws Exception
     */
    public static void sendMail(Map mapConfig, String subject,
            String bodyMessage, String astrfileName) throws Exception {

        try {

            //---------------------variable declaration
            String from;
            String host;
            String port;
            String password;
            String authentication;
            List lstTo;
            Properties properties;
            Session session;
            MimeMessage message;
            BodyPart messageBodyPart;
            Iterator lstIter;
            Multipart multipart;
            Transport transport;

            //---------------------get config data
            from = (String) mapConfig.get(USER_NAME);
            host = (String) mapConfig.get(HOST);
            password = (String) mapConfig.get(PASSWORD);
            port = (String) mapConfig.get(PORT);
            authentication = (String) mapConfig.get(AUTHENTICATION);
            lstTo = (List) mapConfig.get(TO);

            //---------------------get system property
            properties = System.getProperties();

            //---------------------set mail server details in property
            properties.setProperty(HOST_KEY, host);
            properties.setProperty(USER_NAME_KEY, from);
            properties.setProperty(PASSWORD_KEY, password);
            properties.setProperty(PORT_KEY, port);
            properties.setProperty(STARTTLS_KEY, TRUE);
            properties.setProperty(STARTTLS_KEY, FALSE);
            properties.setProperty(AUTHORIZATION_KEY, authentication);
            properties.setProperty(SSL_KEY, TRUE);

            //-----------------------get the default Session object.
            session = Session.getDefaultInstance(properties);

            //-----------------------for debuging
            session.setDebug(true);

            //-------------------------------------create mime object
            message = new MimeMessage(session);

            //-------------------------------------set header value
            message.setFrom(new InternetAddress(from));

            //-------------------------------------set receipient address
            lstIter = lstTo.iterator();

            while (lstIter.hasNext()) {
                message.addRecipient(Message.RecipientType.TO,
                        new InternetAddress((String) lstIter.next()));
            }

            //-------------------------------------set Subject header field
            message.setSubject(subject);

            messageBodyPart = new MimeBodyPart();

            //-------------------------------------fill the message
            messageBodyPart.setText(bodyMessage);

            //-------------------------------------create a multipar message
            multipart = new MimeMultipart();

            //-------------------------------------set text message part
            multipart.addBodyPart(messageBodyPart);

            //-------------------------------------check if attachment is needed
            if (astrfileName != null) {

                //-------------------------------------part two is attachment
                messageBodyPart = new MimeBodyPart();

                String filename = astrfileName;

                DataSource source = new FileDataSource(filename);
                messageBodyPart.setDataHandler(new DataHandler(source));

                messageBodyPart.setFileName(
                        StringUtil.getFileNameFromPath(filename));

                multipart.addBodyPart(messageBodyPart);
            }

            //-------------------------------------send the complete message parts
            message.setContent(multipart);

            //-------------------------------------get transport value
            transport = session.getTransport(SMTP);
            transport.connect(host, from, password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            //-------------------------------------set message
            //  Transport.send(message);

        } finally {
        }
    }

    /**
     *
     * @param log
     */
    public void setLogFile(Logger log) {
    }
}
/* * end class * */