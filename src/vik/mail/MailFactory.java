package vik.mail;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import vik.com.StringUtil;
import vik.common.Constants;
import vik.log.VikLogManager;
import static vik.common.Constants.FALSE;
import static vik.common.Constants.HOST_KEY;
import static vik.common.Constants.PASSWORD_KEY;
import static vik.common.Constants.PORT_KEY;
import static vik.common.Constants.SMTP;
import static vik.common.Constants.STARTTLS_KEY;
import static vik.common.Constants.TRUE;
import static vik.common.Constants.USER_NAME_KEY;
import vik.log.TestLogger;

/**
 *
 * @author vikas
 */
public class MailFactory {

    private static String userName;
    private static String host;
    private static String password;
    private static String port;
    private static List to;
    private static List bcc;
    private static List cc;
    private  String subject;
    private String body;
    private static String attachementFileName;
    private static Properties defaultProperties;
    private static Properties properties;

    public static String getAttachementFileName() {
        return attachementFileName;
    }

    public static void setAttachementFileName(String attachementFileName) {
        MailFactory.attachementFileName = attachementFileName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public MailFactory(Properties properties) {
        this.properties = properties;
    }

    private Logger log = TestLogger.getLogger(MailFactory.class.getName());

    /**
     *
     * @throws java.lang.Exception
     */
//    public MailFactory() throws Exception {
//        VikLogManager logMgr = new VikLogManager();
//        log = logMgr.getLogger(MailFactory.class.getName());
//
//    }
    /**
     * send mail function.
     * <p>
     * @param mapConfig   configuration details
     * @param subject     mail subjects
     * @param bodyMessage mail body
     * <p>
     * @throws Exception
     */
//    public static void sendMail(Map mapConfig, String subject,
//            String bodyMessage) throws Exception {
//        sendMail(mapConfig, subject, bodyMessage, null);
//
//    }
    /**
     * send mail function.
     * <p>
     * @throws Exception
     */
//    public void sendMail(Map mapConfig, String subject,
//            String bodyMessage, String attachementFileName) throws Exception {
    public void sendMail() throws Exception {
        validation();

        try {

            //---------------------variable declaration
            String authentication = null;
            Session session;
            MimeMessage message;
            BodyPart messageBodyPart;
            Iterator lstIter;
            Multipart multipart;
            Transport transport;
            properties.setProperty(STARTTLS_KEY, TRUE);
            log.info("initializing default propertis");
//            if( properties == null ) {
//                initializeDefautProperties();
//                properties = defaultProperties;
//
//            }

//            initializeProperties(properties);
            log.info("getting default session objects");

            //-----------------------get the default Session object.
            session = Session.getDefaultInstance(properties);

            //-----------------------for debuging
            session.setDebug(true);
            log.log(Level.INFO, "session debug mode: {0}", session.getDebug());

            log.info("get mime object");
            //-------------------------------------create mime object
            message = new MimeMessage(session);

            log.info("setting header");
            //-------------------------------------set header value
            message.setFrom(new InternetAddress(properties.getProperty(Constants.USER_NAME_KEY)));

            if( to != null ) {
                //-------------------------------------set receipient address
                lstIter = to.iterator();

                while( lstIter.hasNext() ) {
                    message.addRecipient(Message.RecipientType.TO,
                            new InternetAddress((String) lstIter.next()));
                }
            }

            if( cc != null ) {
                lstIter = cc.iterator();

                while( lstIter.hasNext() ) {
                    message.addRecipient(Message.RecipientType.CC,
                            new InternetAddress((String) lstIter.next()));
                }
            }

            if( bcc != null ) {
                lstIter = bcc.iterator();

                while( lstIter.hasNext() ) {
                    message.addRecipient(Message.RecipientType.BCC,
                            new InternetAddress((String) lstIter.next()));
                }
            }

            subject = (subject == null) ? "" : subject;
            //-------------------------------------set Subject header field
            message.setSubject(subject);

            messageBodyPart = new MimeBodyPart();

            //-------------------------------------fill the message
            body = (body == null) ? "" : body;

            messageBodyPart.setText(body);

            //-------------------------------------create a multipar message
            multipart = new MimeMultipart();

            //-------------------------------------set text message part
            multipart.addBodyPart(messageBodyPart);

            //-------------------------------------check if attachment is needed
            if( attachementFileName != null ) {
                log.info("setting attachments...");

                //-------------------------------------part two is attachment
                messageBodyPart = new MimeBodyPart();

                String filename = attachementFileName;

                DataSource source = new FileDataSource(filename);
                messageBodyPart.setDataHandler(new DataHandler(source));

                messageBodyPart.setFileName(
                        StringUtil.getFileNameFromPath(filename));

                multipart.addBodyPart(messageBodyPart);
            }

            //-------------------------------------send the complete message parts
            message.setContent(multipart);

            //-------------------------------------get transport value
            transport = session.getTransport(SMTP);
            transport.connect(host, userName, password);
            log.info("sending mail ...");
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            log.info("Mail Sent Successfully... ");
            //-------------------------------------set message
            //  Transport.send(message);
        } finally {
        }
    }

    private Properties getDefaultProperties() {
        initializeDefautProperties();
        defaultProperties.put("KEY_SOURCE", true);
        return defaultProperties;
    }

    private void setDefaultProperties(Properties defaultProperties) {
        MailFactory.defaultProperties = defaultProperties;
    }

    /**
     *
     * @return
     */
    public Properties getProperties() {

        return getDefaultProperties();
    }

    /**
     *
     * @param properties <p>
     * @throws Exception
     */
    public void setProperties(Properties properties) throws Exception {
        if( !(boolean) properties.get("KEY_SOURCE") ) {
            throw new Exception("Property only from the getCustomeProperties() method can be accepted");
        }

        MailFactory.properties = properties;
    }

    /**
     *
     * @param log
     */
    public void setLogFile(Logger log) {
    }

    /**
     *
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     *
     * @return
     */
    public String getHost() {
        return host;
    }

    /**
     *
     * @param host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    public String getPort() {
        return this.port;
    }

    /**
     *
     * @param port
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     *
     * @return
     */
    public List getTo() {
        return to;
    }

    /**
     *
     * @param to
     */
    public void setTo(List to) {
        this.to = to;
    }

    /**
     *
     * @return
     */
    public List getBcc() {
        return bcc;
    }

    /**
     *
     * @param bcc
     */
    public void setBcc(List bcc) {
        this.bcc = bcc;
    }

    /**
     *
     * @return
     */
    public List getCc() {
        return cc;
    }

    /**
     *
     * @param cc
     */
    public void setCc(List cc) {
        this.cc = cc;
    }

    private void initializeDefautProperties() {
        //---------------------get system property
        defaultProperties = System.getProperties();
        defaultProperties.setProperty(STARTTLS_KEY, TRUE);
        defaultProperties.setProperty(STARTTLS_KEY, FALSE);

        //---------------------set mail server details in property
//            properties.setProperty(HOST_KEY, host);
//            properties.setProperty(USER_NAME_KEY, from);
//            properties.setProperty(PASSWORD_KEY, password);
//            properties.setProperty(PORT_KEY, port);
//            properties.setProperty(STARTTLS_KEY, TRUE);
//            properties.setProperty(STARTTLS_KEY, FALSE);
//            properties.setProperty(AUTHORIZATION_KEY, authentication);
//            properties.setProperty(SSL_KEY, TRUE);
//            properties.setProperty(AUTHORIZATION_KEY, authentication);
//            properties.setProperty(SSL_KEY, TRUE);
    }

    private void initializeProperties(final Properties properties) {

        properties.setProperty(HOST_KEY, host);
        properties.setProperty(USER_NAME_KEY, userName);
        properties.setProperty(PASSWORD_KEY, password);
        properties.setProperty(PORT_KEY, port);

    }

    private void validation() throws Exception {

        userName = properties.getProperty(USER_NAME_KEY);
        password = properties.getProperty(PASSWORD_KEY);
        host = properties.getProperty(HOST_KEY);
        port = properties.getProperty(PORT_KEY);

        if( userName == null ) {
            throw new Exception("userName is blank");
        }
        if( password == null ) {
            throw new Exception("password is blank");
        }

        if( host == null ) {
            throw new Exception("host is blank");

        }
        if( port == null ) {
            throw new Exception("port is blank");
        }
        if( to == null && cc == null && bcc == null ) {
            throw new Exception("receipient address is blank");
        }

    }
}
/*
 * * end class *
 */
