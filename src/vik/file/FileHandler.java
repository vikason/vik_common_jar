package vik.file;

import vik.common.Constants;

/**
 *
 * @author vikas
 */
public class FileHandler {

    /**
     * Return then name of OS
     * <p>
     * @return Name of OS.
     */
    public static String getOsName() {
        return System.getProperty("os.name");
    }

    /**
     * Return true if operating system is windows.
     * <p>
     * @return
     */
    public static boolean isWindows() {
        return (getOsName().toLowerCase().startsWith("window"));
    }

    /**
     * Return true if operating system is linux.
     * <p>
     * @return
     */
    public static boolean isLinux() {
        return (getOsName().toLowerCase().startsWith("linux"));
    }

    /**
// * Get direcotry file path separator for depend on OS.
     * <p>
     * @return
     */
    public static String getDirSeparater() {

        return (isWindows())
                ? Constants.KEY_WINDOWS_DIR_SEP
                : Constants.KEY_LINUX_DIR_SEP;
    }

}
