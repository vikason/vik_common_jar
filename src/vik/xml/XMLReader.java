package vik.xml;

import vik.xml.com.XMLHandler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class XMLReader {

    private String filePath;

    private XMLHandler handler;

    /**
     *
     * @param filePath
     * @param handler
     */
    public XMLReader(String filePath,
            XMLHandler handler) {
        this.filePath = filePath;
        this.handler = handler;
    }

    /**
     *
     * @return
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     *
     * @param filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     *
     * @return
     */
    public XMLHandler getHandler() {
        return handler;
    }

    /**
     *
     * @param handler
     */
    public void setHandler(XMLHandler handler) {
        this.handler = handler;
    }

    /**
     *
     * @return @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public Map readXMLData()
            throws ParserConfigurationException, SAXException, IOException {

        Map mapData = new HashMap(0);

        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            saxParser.parse(filePath, handler);
            mapData = handler.getXMLData();
        } finally {
        }

        return mapData;
    } // readXMLData

}
