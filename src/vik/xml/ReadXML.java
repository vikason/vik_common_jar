/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vik.xml;

import vik.xml.handler.MailConfigXMLHandler;
import vik.xml.com.XMLHandler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @Desc:Java Class
 * @author : Vikas Kumar Verma
 * <p>
 * use XMLReader instead
 */
@Deprecated
public class ReadXML {

    /**
     * default constructor
     */
    public ReadXML() {
    }

    /**
     * This function returns the parameters specified in the excel file
     * <p>
     * @param filePath: absolute path of the file like (D:\file.xls)
     * <p>
     * @return Map
     * <p>
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    /*
     * example: <?xml version="1.0"?> <SUNSEAL-CONFIG> <EMAIL>
     * <username>vikas.on.app@gmail.com</username>
     * <password>raghunandan123</password> <host>smtp.gmail.com</host>
     * <port>587</port> <authentication>true</authentication> </EMAIL>
     * </SUNSEAL-CONFIG>
     */
    public static Map readMailConfig(String filePath)
            throws ParserConfigurationException, SAXException, IOException {

        Map mapConfigValue = new HashMap(0);

        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            XMLHandler handler = new MailConfigXMLHandler();

            saxParser.parse(filePath, handler);
            mapConfigValue = handler.getXMLData();
        } finally {
        }

        return mapConfigValue;
    } // readMailConfig

}
/*
 * end of class
 */
