package vik.xml.handler;

import java.util.Stack;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import vik.xml.com.XMLHandler;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class GeneralHandler extends XMLHandler {

    private final Stack stack = new Stack();

    @Override
    public void characters(char[] ch,
            int start,
            int length) throws SAXException {

        String str = new String(ch, start, length);
        str = str.replaceAll("\\n", "");
        if( str.trim().length() != 0 ) {
            /**
             * add data to map
             */
            addData((String) stack.pop(), str);
        }
    }

    @Override
    protected String getBeginingTagName() {
        return "SUNSEAL-CONFIG";
    }

    @Override
    public void readStartElement(String uri,
            String localName,
            String qName,
            Attributes attributes) throws SAXException {

        stack.push(qName);

    }

    @Override
    public void readEndElement(String uri,
            String localName,
            String qName) throws SAXException {
        //  System.out.println(stack.toString());

    }

}
