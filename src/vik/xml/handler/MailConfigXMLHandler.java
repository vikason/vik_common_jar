package vik.xml.handler;

import vik.xml.com.XMLHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import static vik.common.Constants.AUTHENTICATION;
import static vik.common.Constants.HOST;
import static vik.common.Constants.PASSWORD;
import static vik.common.Constants.PORT;
import static vik.common.Constants.USER_NAME;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class MailConfigXMLHandler extends XMLHandler {

    boolean isUserName = false;
    boolean isPassword = false;
    boolean isHost = false;
    boolean isPort = false;
    boolean isAuthentication = false;

    @Override
    public void readStartElement(String uri,
            String localName,
            String qName,
            Attributes attributes) throws SAXException {

        if ( qName.equalsIgnoreCase(USER_NAME) ) {
            isUserName = true;
        }

        if ( qName.equalsIgnoreCase(PASSWORD) ) {
            isPassword = true;
        }

        if ( qName.equalsIgnoreCase(HOST) ) {
            isHost = true;
        }

        if ( qName.equalsIgnoreCase(PORT) ) {
            isPort = true;
        }

        if ( qName.equalsIgnoreCase(AUTHENTICATION) ) {
            isAuthentication = true;
        }

    }

    @Override
    public void characters(char ch[],
            int start,
            int length) throws SAXException {

        elementValue = new String(ch, start, length);
        if ( isUserName ) {
            isUserName = false;
//            mapXMLData.put(USER_NAME, elementValue);
            addData(USER_NAME, elementValue);
            return;
        }

        if ( isPassword ) {
//            mapXMLData.put(PASSWORD, elementValue);
            isPassword = false;
            addData(PASSWORD, elementValue);
            return;
        }

        if ( isHost ) {
//            mapXMLData.put(HOST, elementValue);
            addData(HOST, elementValue);
            isHost = false;
            return;
        }

        if ( isPort ) {
//            mapXMLData.put(PORT, elementValue);
            addData(PORT, elementValue);
            isPort = false;
            return;
        }

        if ( isAuthentication ) {
//            mapXMLData.put(AUTHENTICATION, elementValue);
            addData(AUTHENTICATION, elementValue);
            isAuthentication = false;
        }
    }

    @Override
    protected String getBeginingTagName() {

        return "SUNSEAL-CONFIG";
    }

    @Override
    public void readEndElement(String uri,
            String localName,
            String qName) throws SAXException {

    }
}
