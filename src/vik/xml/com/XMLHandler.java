package vik.xml.com;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public abstract class XMLHandler extends DefaultHandler {

    /**
     *
     */
    protected Map mapXMLData = new HashMap();
    private final Stack stack = new Stack();
    private List listElementData = new ArrayList();
    private String parentTagName;
    private boolean isRemoveSpace = false;

    public boolean isIsRemoveSpace() {
        return isRemoveSpace;
    }

    public void setIsRemoveSpace(boolean isRemoveSpace) {
        this.isRemoveSpace = isRemoveSpace;
    }

    //  private static int count = 1;
    /**
     *
     */
    protected String elementValue;

    /**
     *
     * @return
     */
    public Map getXMLData() {
        return mapXMLData;
    }

    @Override
    public void endElement(String uri,
            String localName,
            String qName) throws SAXException {
        readEndElement(uri, localName, qName);
        if( !getBeginingTagName().equals(qName) ) {
            parentTagName
                    = String.valueOf(stack.pop());

            //       parentTagName = parentTagName + String.valueOf(count);
            parentTagName = parentTagName;
//            count++;
            addDataToMap();
        }
    }

    @Override
    public void startElement(String uri,
            String localName,
            String qName,
            Attributes attributes) throws SAXException {
        if( !getBeginingTagName().equals(qName) ) {
            stack.push(qName);
        }

        readStartElement(uri, localName, qName, attributes);
    }

    @Override
    public abstract void characters(char ch[],
            int start,
            int length) throws SAXException;

    /**
     *
     * @return
     */
    protected abstract String getBeginingTagName();

    /**
     *
     * @param uri
     * @param localName
     * @param qName
     * @param attributes <p>
     * @throws SAXException
     */
    public abstract void readStartElement(String uri,
            String localName,
            String qName,
            Attributes attributes) throws SAXException;

    /**
     *
     * @param uri
     * @param localName
     * @param qName     <p>
     * @throws SAXException
     */
    public abstract void readEndElement(String uri,
            String localName,
            String qName) throws SAXException;

    /**
     *
     * @param key
     * @param value
     */
    protected void addData(String key,
            String value) {

        value = value.replaceAll("\\n", "");

        if( isRemoveSpace ) {
            value = value.replaceAll("\\t", "    ");
            value = value.replaceAll(" ", "");
        }
        XMLDataBean bean = new XMLDataBean(key, value);
        listElementData.add(bean);
        addDataToMap();
    }

    /**
     *
     */
    protected void printXmlData() {
        System.out.println("1:::" + mapXMLData);

    }

    private void addDataToMap() {
        if( stack.empty() ) {

            mapXMLData.put(parentTagName, listElementData);
            //  count++;
            listElementData = new ArrayList();
        }
    }
}
