package vik.xml.com;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class XMLDataBean {

    private String tag;
    private String value;

    public XMLDataBean() {
    }

    public XMLDataBean(String tag,
            String value) {
        this.tag = tag;
        this.value = value;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "XMLDataBean{" + "tag=" + tag + ", value=" + value + '}';
    }

}
