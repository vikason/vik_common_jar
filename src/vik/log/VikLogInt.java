/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vik.log;

import java.util.logging.Logger;

/**
 *
 * @author Vikas Verma
 */
@Deprecated
public interface VikLogInt {

    /**
     *
     * @return
     * @deprecated
     */
    @Deprecated
    public Logger getLogger();

//    public Logger getFileLogger(String loggerName, String logFilePath);

    /**
     *
     * @param logger
     */
    
    public void setLogger(Logger logger);

    /**
     *
     * @param logger
     */
    public void closeLogger(Logger logger);
}
