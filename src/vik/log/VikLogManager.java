package vik.log;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import vik.common.Constants;
import vik.file.FileHandler;

/**
 *
 * @author vikas
 */
public final class VikLogManager {

    private String logDir;
    private Level debugLevel;
    private Level fileLevel;
    private String logFileExtention;
    private boolean addTimeStamp;
    /**
     *
     */
    public VikLogManager() {
        logDir = Constants.LOG_DIR;
        debugLevel = Constants.DEBUG_LEVEL;
        fileLevel = Constants.FILE_DEBUG_LEVEL;
        logFileExtention = Constants.LOG_FILE_EXTENTION;
        addTimeStamp = Constants.ADD_TIME_STAMP;

    }

    /**
     *
     * @param logDir
     */
    public void setLogDir(String logDir) {
        this.logDir = logDir;
    }

    public boolean isAddTimeStamp() {
        return addTimeStamp;
    }

    public void setAddTimeStamp(boolean addTimeStamp) {
        this.addTimeStamp = addTimeStamp;
    }

    /**
     *
     * @return
     */
    public Level getDebugLevel() {
        return debugLevel;
    }

    /**
     *
     * @param debugLevel
     */
    public void setDebugLevel(Level debugLevel) {
        this.debugLevel = debugLevel;
    }

    /**
     *
     * @return
     */
    public Level getFileLevel() {
        return fileLevel;
    }

    /**
     *
     * @param fileLevel
     */
    public void setFileLevel(Level fileLevel) {
        this.fileLevel = fileLevel;
    }

    /**
     * Get file directory path for logging.
     * <p>
     * @return
     */
    public String getLogDir() {
        return new File("").getAbsolutePath()
                + FileHandler.getDirSeparater()
                + logDir;
    }

    /**
     * Get logger name for logging.
     * <p>
     * @param loggerName logger Name
     * <p>
     * @return Logger logger for logging.
     * <p>
     * @throws java.lang.Exception
     */
    public Logger getLogger(String loggerName) throws Exception {
        return getLogger(loggerName, false);
    }

    /**
     * Return logger object.
     * <p>
     * @param loggerName   name of logger object
     * @param isFileLogged true if need to log into file.<p>
     * @return @throws java.lang.Exception
     */
    public Logger getLogger(String loggerName,
            boolean isFileLogged) throws Exception {
        VikLoggerFactory obj;

        obj = new VikLoggerFactory();
        obj.setDebutLevel(debugLevel);
        obj.setFileDebugLevel(fileLevel);
        obj.setAddTimeStamp(addTimeStamp);
        obj.setLogFileExtention(logFileExtention);

        if ( isFileLogged ) {
            return obj.getFileLogger(loggerName,
                    getLogDir());
        } else {
            return obj.getFileLogger(loggerName);
        }
    }

    /**
     *
     * @return
     */
    public String getLogFileExtention() {
        return logFileExtention;
    }

    /**
     *
     * @param logFileExtention
     */
    public void setLogFileExtention(String logFileExtention) {
        this.logFileExtention = logFileExtention;
    }
}
