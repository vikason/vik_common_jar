package vik.log;

import java.util.logging.Level;
import java.util.logging.Logger;
import vik.common.Constants;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class TestLogger implements Constants {

    /**
     * Class to get logger object
     * <p>
     * @param loggerName name of the logger
     * <p>
     * @return Logger object
     * <p>
     */
    public static Logger getLogger(String loggerName) {

        VikLogManager logmgr;
        Logger log = null;
        logmgr = new VikLogManager();

        logmgr.setDebugLevel(DEBUG_LEVEL);
        logmgr.setFileLevel(FILE_DEBUG_LEVEL);
        logmgr.setLogDir(LOG_DIR);
        logmgr.setLogFileExtention(LOG_FILE_EXT);
        try {
            log = logmgr.getLogger(loggerName, false);
        } catch(Exception ex) {
            Logger.getLogger(TestLogger.class.getName()).log(Level.SEVERE, null, ex);
        }

        return log;
    }
}
