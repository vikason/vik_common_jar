package vik.log;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import vik.com.StringUtil;
import vik.common.Constants;

/**
 *
 * @author Vikas Verma
 */
public class VikLoggerFactory {

    static private FileHandler fileHandler;
    static private SimpleFormatter simpleFormatter;

    /**
     *
     */
    protected static Logger logger;
    private Level DebugLevel;
    private Level fileDebugLevel;
    private boolean addTimeStamp = false;
    private String timeFormat;
    private String logFileExtention;

    /**
     * Default constructor
     */
    public VikLoggerFactory() {
    }

    /**
     * Set logger name and initialize Logger object.
     * <p>
     * @param loggerName name of logger.
     */
    public VikLoggerFactory(String loggerName) {
        this.setLogger(loggerName);
    }

    /**
     * Return logging level for file.
     * <p>
     * @return Logging level.
     */
    public Level getFileDebugLevel() {
        return fileDebugLevel;
    }

    /**
     * Set logging level for file.
     * <p>
     * @param fileDebugLevel
     */
    public void setFileDebugLevel(Level fileDebugLevel) {
        this.fileDebugLevel = fileDebugLevel;
    }

    /**
     *
     * @return
     */
    public String getLogFileExtention() {
        return logFileExtention;
    }

    /**
     *
     * @param logFileExtention
     */
    public void setLogFileExtention(String logFileExtention) {
        this.logFileExtention = logFileExtention;
    }

    /**
     * Returns the <code>Logger</code> object to log the error
     * <p>
     * @param loggerName: Name of the logger,log will create on C: drive
     * <p>
     * @return <code>Logger</code>
     * <p>
     */
    @Deprecated
    public static Logger getLogger(String loggerName) {
        return getLogger(loggerName, null);
    } //getLogger

    /**
     * Returns the <code>Logger</code> object to log the error
     * <p>
     * @param loggerName:  Name of the logger
     * @param logFilePath: absolute file path to log the exception.
     * <p>
     * @return <code>Logger</code>
     * <p>
     * Deprecated method, use getFileLogger Instead
     */
    @Deprecated
    public static Logger getLogger(String loggerName,
            String logFilePath) {

        String filePath;
        try {

            //--------------------------get new logger instance
            logger = Logger.getLogger(loggerName);
            //--------------------------get new level of logger instance
            logger.setLevel(Constants.DEBUG_LEVEL);

            loggerName = loggerName.replace(".", "~");

            if( logFilePath != null ) {
                //--------------------------set file in which logging should be logged
                filePath = logFilePath + "\\"
                        + loggerName + "_" + StringUtil.getCurrentTimeStamp() + ".log";

                fileHandler = new FileHandler(filePath);

                //---------------------------Create txt Formatter
                simpleFormatter = new SimpleFormatter();

                //---------------------set foramt in file handler
                fileHandler.setFormatter(simpleFormatter);

                //---------------------set add handler in logger object
                logger.addHandler(fileHandler);
            }

        } catch(SecurityException | IOException e) {
            e.printStackTrace();
        }

        //--------------------return logger object
        return logger;
    } // getLogger

    /**
     * Close the log handler attached to passed logger.
     * <p>
     * @param logger
     */
    public static void closeLoggerHandler(Logger logger) {
        for( Handler h : logger.getHandlers() ) {
            h.close();
        }
    } //closeLoggerHandler

    /**
     * Return Logger object.
     * <p>
     * @param loggerName <p>
     * @return logger object
     * <p>
     * @throws java.lang.Exception
     */
    public Logger getFileLogger(String loggerName) throws Exception {
        return getFileLogger(loggerName, null);
    }

    /**
     * Return File logger.
     * <p>
     * @param loggerName  logger name
     * @param logFilePath log file path<p>
     * @return Logger
     * <p>
     * @throws java.io.IOException
     */
    public Logger getFileLogger(String loggerName,
            String logFilePath) throws IOException, Exception {

        String filePath;
        StringBuilder strBuild = new StringBuilder("");
        String strDirSep;

        try {

            /**
             * some validation before creating logger object.
             */
            loggerValidation(loggerName);

            /**
             * get new logger instance
             */
            logger = Logger.getLogger(loggerName);

            logger.setUseParentHandlers(false);

            /**
             * initialize debug level for this logger.
             */
            initializingParameters();

            /**
             * handler need to log fine and fines message.
             */
            addConsoleHandler(Level.FINEST);

            /**
             * set file operations if logging into file.
             */
            if( logFilePath != null ) {
                strDirSep = vik.file.FileHandler.getDirSeparater();

                //--------------------------set file in which logging should be logged
                if( addTimeStamp ) {
                    if( timeFormat != null ) {
                        strBuild.append(logFilePath);
                        strBuild.append(strDirSep);
                        strBuild.append(loggerName);
                        strBuild.append("_");
                        strBuild.append(StringUtil.getCurrentTimeStamp(timeFormat));
                        strBuild.append(".");
                        strBuild.append(logFileExtention);
//                        filePath = logFilePath + strDirSep
//                                + loggerName + "_"
//                                + StringUtil.getCurrentTimeStamp(timeFormat)
//                                + "." + logFileExtention;
                    } else {
//                        filePath = logFilePath
//                                + strDirSep
//                                + loggerName
//                                + "_"
//                                + StringUtil.getCurrentTimeStamp()
//                                + "."
//                                + logFileExtention;

                        strBuild.append(logFilePath);
                        strBuild.append(strDirSep);
                        strBuild.append(loggerName);
                        strBuild.append("_");
                        strBuild.append(StringUtil.getCurrentTimeStamp());
                        strBuild.append(".");
                        strBuild.append(logFileExtention);
                    }

                } else {
//                    filePath = logFilePath
//                            + strDirSep
//                            + loggerName
//                            + "."
//                            + logFileExtention;

                    strBuild.append(logFilePath);
                    strBuild.append(strDirSep);
                    strBuild.append(loggerName);
                    strBuild.append(".");
                    strBuild.append(logFileExtention);
                }

                filePath = strBuild.toString();

                //--------------------------set file handler
                fileHandler = new FileHandler(filePath);

                fileHandler.setLevel(fileDebugLevel);

                //---------------------------Create txt Formatter
                simpleFormatter = new SimpleFormatter();

                //---------------------set foramt in file handler
                fileHandler.setFormatter(simpleFormatter);

                //---------------------set add handler in logger object
                logger.addHandler(fileHandler);

            }

        } finally {

        }

        /**
         * return logger object.
         */
        return logger;
    } // getFileLogger

    /**
     * return file handler.
     * <p>
     * @return
     */
    public static FileHandler getFileHandler() {
        return fileHandler;
    }

    /**
     * set file handler.
     * <p>
     * @param fileHandler
     */
    public static void setFileHandler(FileHandler fileHandler) {
        VikLoggerFactory.fileHandler = fileHandler;
    }

    /**
     * get status of add timestamp status flag value
     * <p>
     * @return
     */
    public boolean isAddTimeStamp() {
        return addTimeStamp;
    }

    /**
     * if set to true then add time stamp in logging file.
     * <p>
     * @param addTimeStamp
     */
    public void setAddTimeStamp(boolean addTimeStamp) {
        this.addTimeStamp = addTimeStamp;
    }

    /**
     * return debug level
     * <p>
     * @return
     */
    public Level getDebugLevel() {
        return DebugLevel;
    }

    /**
     * set default debug level.
     * <p>
     * @param DebutLevel
     */
    public void setDebutLevel(Level DebutLevel) {
        this.DebugLevel = DebutLevel;
    }

    /**
     * Initialize logger
     * <p>
     * @param loggerName - logger name
     */
    public final void setLogger(String loggerName) {
        VikLoggerFactory.logger = Logger.getLogger(loggerName);
        logger.setLevel(Constants.DEBUG_LEVEL);

    }

    /**
     *
     * @return
     */
    public String getTimeFormat() {
        return timeFormat;
    }

    /**
     *
     * @param timeFormat
     */
    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
        this.setAddTimeStamp(true);
    }

    private void loggerValidation(String loggerName) throws Exception {
        if( (loggerName == null) || (loggerName.trim().equals("")) ) {
            throw new Exception("Logger name can not be blank");
        }
    }

    private void addConsoleHandler(Level level) {
        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(level);

        removeAllHandler();
        logger.setLevel(getDebugLevel());
        logger.addHandler(consoleHandler);
    }

    private void initializingParameters() {
        /**
         * initialize logger debug level
         */
        if( getDebugLevel() == null ) {
            setDebutLevel(Constants.DEBUG_LEVEL);
        }

        /**
         * initialize log file extention.
         */
        if( getLogFileExtention() == null ) {
            setLogFileExtention(Constants.LOG_FILE_EXTENTION);
        }
        /**
         * initialize file logger debug level
         */
        if( fileDebugLevel == null ) {
            fileDebugLevel = Constants.FILE_DEBUG_LEVEL;
        }
    }

    private void removeAllHandler() {
        Handler[] hand = logger.getHandlers();
        for( int i = 0; i < hand.length; i++ ) {
            logger.removeHandler(hand[i]);
        }
    }
} // VikLoggerFactory

/*
 * end of class
 */
