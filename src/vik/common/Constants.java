package vik.common;

import java.util.logging.Level;

/**
 *
 * @Desc:Java Class
 * @author : Vikas Kumar Verma
 */
public interface Constants {

    /**
     *
     */
    public static final String HOST_KEY = "smtp.gmail.com";

    /**
     *
     */
    public static final String USER_NAME_KEY = "mail.smtp.user";

    /**
     *
     */
    public static final String PASSWORD_KEY = "mail.smtp.password";

    /**
     *
     */
    public static final String PORT_KEY = "mail.smtp.port";

    /**
     *
     */
    public static final String STARTTLS_KEY = "mail.smtp.starttls.enable";

    /**
     *
     */
    public static final String AUTHORIZATION_KEY = "mail.smtp.auth";

    /**
     *
     */
    public static final String SSL_KEY = "mail.smtp.ssl.enable";

    /**
     *
     */
    public static final String TRUE = "true";

    /**
     *
     */
    public static final String FALSE = "false";

    /**
     *
     */
    public static final String SMTP = "smtp";

    /**
     *
     */
    public static final String USER_NAME = "username";

    /**
     *
     */
    public static final String PASSWORD = "password";

    /**
     *
     */
    public static final String HOST = "host";

    /**
     *
     */
    public static final String PORT = "port";

    /**
     *
     */
    public static final String AUTHENTICATION = "authentication";

    /**
     *
     */
    public static final String TO = "to";

    /**
     *
     */
    public static final String CC = "cc";

    /**
     *
     */
    public static final String BCC = "bcc";

    /**
     *
     */
    public static final String DATE_FORMAT = "yyyyMMddHmmss";
//    public static final String LOG_DIR = "\\log";

    /**
     *
     */
    public static final String KEY_LOGGER = "KEY_LOGGER";

    /**
     *
     */
    public static final String KEY_WINDOWS_DIR_SEP = "\\";

    /**
     *
     */
    public static final String KEY_LINUX_DIR_SEP = "/";

    /**
     *
     */
    public static final String KEY_DB_DRIVER = "KEY_DB_DRIVER";

    /**
     *
     */
    public static final String KEY_DB_CON_STRING = "KEY_DB_CON_STRING";

    /**
     *
     */
    public static final String LOG_FILE_EXTENTION = "log";

    /**
     *
     */
    public static final String LOG_DIR = "log";

    /**
     *
     */
    public static final Level DEBUG_LEVEL = Level.ALL;

    /**
     *
     */
    public static final Level FILE_DEBUG_LEVEL = Level.SEVERE;

    /**
     *
     */
    public static final String LOG_FILE_EXT = "txt";
    public static boolean ADD_TIME_STAMP = false;

}
/**
 * Expression end is undefined on line 19, column 4 in
 * Templates/Classes/Class.java.
 */
