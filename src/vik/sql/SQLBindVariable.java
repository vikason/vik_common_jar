/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vik.sql;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import vik.com.StringUtil;
import vik.log.TestLogger;
import vik.sql.CredentialApp.MySqlCons;
import vik.sql.exception.BadSqlFormatException;

/**
 *
 * @author VIKAS
 */
public class SQLBindVariable {

    private static Logger log = Logger.getLogger(SQLBindVariable.class.getName());

    private static String SQL;

//    public static String bindSql(final String SQL, final Map parameter) throws BadSqlFormatException {

    /**
     *
     */
        public SQLBindVariable() {
        try {
            log = TestLogger.getLogger(SQLBindVariable.class.getName());
        } catch(Exception ex) {
            Logger.getLogger(SQLBindVariable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param map
     * @return
     * @throws BadSqlFormatException
     */
    public static String bindSql(final Map map) throws BadSqlFormatException {

        String lstrSql;
        String key;
        Map parameter;
        boolean isBindVar;
        int bindCount;
        int index;
        StringBuilder str;

        SQL = (String) map.get(MySqlCons.KEY_SQL);
        lstrSql = SQL;
        parameter = (Map) map.get(MySqlCons.KEY_SQL_PARAMETER);
        // log = (Logger) map.get(MySqlCons.KEY_LOGGER);

        isBindVar = false;
        bindCount = 0;
        index = 0;

        if( lstrSql.contains("?") ) {
            isBindVar = true;
        }

        str = new StringBuilder("");

        try {
            if( isBindVar ) {

                for( int i = 1; i <= 1000; i++ ) {
                    key = String.valueOf(i);
                    if( parameter.containsKey(key) ) {
                        bindCount++;
                        index = 0;
                        str.append(lstrSql.substring(index, lstrSql.indexOf("?") - 1));
                        index = index + lstrSql.indexOf("?");

                        String bindValue = (String) parameter.get(key);
                        if( bindValue == null ) {
                            str.append(" null ");
                        } else {
                            str.append("'");
                            str.append((String) parameter.get(key));
                            str.append("'");
                        }

                        lstrSql = lstrSql.substring(index + 1, lstrSql.length());
                    } else {
                        break;
                    }
                }
            }
        } catch(StringIndexOutOfBoundsException ex) {
            bindCount--;
            throw new BadSqlFormatException(SQL, bindCount, parameter.size());
        }

        str = str.append(lstrSql);
        str = new StringBuilder(StringUtil.removeExtSpace(str.toString()));

//        log.info(StringUtil.removeExtSpace(SQL));
//        log.info("===========================================");
        log.info(str.toString());
//        log.info("===========================================");

        lstrSql = SQL;

        validateSql(lstrSql, bindCount, parameter.size());
        return str.toString();
    }

    /**
     *
     * @param lstrSql
     * @param bindCount
     * @param parameterCount <p>
     * @throws BadSqlFormatException
     */
    private static void validateSql(String lstrSql,
            final int bindCount,
            final int parameterCount) throws BadSqlFormatException {
        int index;
        int c = 0;

        while( true ) {
            if( lstrSql.contains("?") ) {
                c++;
                index = lstrSql.indexOf("?");
                lstrSql = lstrSql.substring(index + 1, lstrSql.length());

            } else {
                break;
            }
        }

        if( bindCount != c ) {
            throw new BadSqlFormatException(SQL, c, parameterCount);
        }
    }

    /**
     *
     * @param sql
     * @param parameter
     * <p>
     * @return
     * <p>
     * @throws BadSqlFormatException
     */
    public static String bindSql(final String sql,
            List parameter) throws BadSqlFormatException {
        try {
            log = TestLogger.getLogger(SQLBindVariable.class.getName());
        } catch(Exception ex) {
            Logger.getLogger(SQLBindVariable.class.getName()).log(Level.SEVERE, null, ex);
        }

        int countOfQ = 0;
        int countOFParmeter = 0;
        int index;

        String lstrSql;
        boolean isBindVar;

        StringBuilder sb;

        lstrSql = sql;

        isBindVar = false;

        index = 0;
        countOFParmeter = parameter.size();

        if( lstrSql.contains("?") ) {
            isBindVar = true;
        }
        while( true ) {
            if( lstrSql.contains("?") ) {
                countOfQ++;
                index = lstrSql.indexOf("?");
                lstrSql = lstrSql.substring(index + 1, lstrSql.length());

            } else {
                break;
            }
        }

        if( countOfQ != parameter.size() ) {
            log.warning("Parameter count mismatched.");
            log.log(Level.WARNING, "Parameter founds: {0}", parameter.size());
            log.log(Level.WARNING, "Parameter expected: {0}", countOfQ);
//            throw new BadSqlFormatException("Parameter count mismatched");
        }

        log.log(Level.FINE, "Patameters passed: {0}", parameter.toString());

        lstrSql = sql;
        sb = new StringBuilder("");
        try {
            if( isBindVar ) {
                int startInddex = 0;
                int endIndex = 0;

                for( int i = 0; i < countOFParmeter; i++ ) {
                    endIndex = lstrSql.indexOf("?");
                    sb.append(lstrSql.substring(startInddex, endIndex));
                    sb.append("\"");
                    sb.append((String) parameter.get(i));
                    sb.append("\"");
                    lstrSql = lstrSql.substring(endIndex + 1, lstrSql.length());
                }
            } else {
                sb = new StringBuilder(sql);
            }
        } finally {

        }

        log.info(sb.toString());
        return sb.toString();
    }

}
