package vik.sql;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vik.log.TestLogger;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class TestDB extends DbInterface {

    private Logger log;

    /**
     *
     */
    public TestDB() {
        try {
            this.log = TestLogger.getLogger(TestDB.class.getName());
        } catch(Exception ex) {
            Logger.getLogger(TestDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     */
    public void dbConnectionTest() {
        /**
         * set connection properties.
         */
        try {
            ps = getPreparedStatement("select now()");
        } catch(ClassNotFoundException | SQLException ex) {
            log.log(Level.SEVERE, null, ex);
        }
        try {
            rs = ps.executeQuery();
        } catch(SQLException ex) {
            log.log(Level.SEVERE, null, ex);
        }
        try {
            if( rs.next() ) {
                log.fine(rs.getString(1));
            }
        } catch(SQLException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }
}
