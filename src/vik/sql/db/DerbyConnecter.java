package vik.sql.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vik.log.TestLogger;
import vik.sql.DbConnecter;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class DerbyConnecter extends DbConnecter {

    private Logger log = Logger.getLogger(DerbyConnecter.class.getName());

    /**
     * Default constructor
     * <p>
     * @param database
     * @param user
     * @param password
     * @param isNewDb
     */
    public DerbyConnecter(String database,
            String user,
            String password,
            boolean isNewDb) {


        setDatabase(database);
        setUser(user);
        setPassword(password);
        setCreate(isNewDb);

        setDriverName(EMBEDED_DERBY_DRIVER);
        setUri(EMBEDED_DERBY_URI);

        try {
            log = TestLogger.getLogger(DerbyConnecter.class.getName());
        } catch(Exception ex) {
            Logger.getLogger(DerbyConnecter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getConnectString() {
        StringBuilder sb = new StringBuilder("");
        sb.append(getUri());
        sb.append("\\\\");
        sb.append(getDatabase());
        sb.append(";");
        sb.append("create=");
        sb.append((isCreate()) ? "true" : "false");
        sb.append(";");
        sb.append("user=");
        sb.append(getUser());
        sb.append(";password=");
        sb.append(getPassword());
        sb.append(getOtherSetting());

        log.log(Level.INFO, "Connection String {0}", sb.toString());

        return sb.toString();

    }

    @Override
    public void valid() throws SQLException {
        if( getHost() == null ) {
            throw new SQLException("Host cannot be blank");
        }

        if( getUser() == null ) {
            throw new SQLException("User name cannot be blank");
        }

        if( getPassword() == null ) {
            throw new SQLException("Password cannot be blank");
        }
        if( getDatabase() == null ) {
            throw new SQLException("Database name cannot be blank");
        }
    }

}
