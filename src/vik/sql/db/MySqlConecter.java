package vik.sql.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vik.log.TestLogger;
import vik.sql.DbConnecter;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class MySqlConecter extends DbConnecter {

    private String dbName;
    private String user;
    private String password;

    private Logger log = Logger.getLogger(MySqlConecter.class.getName());

    /**
     * default constructor
     * <p>
     * @param host
     * @param port
     * @param database
     * @param user
     * @param password
     */
    public MySqlConecter(String host,
            String port,
            String database,
            String user,
            String password) {

        setHost(host);
        setPort(port);
        setDatabase(database);
        setUser(user);
        setPassword(password);

        setDriverName(MySQL_DRIVER);
        setUri(MySQL_URI);

        try {
            log = TestLogger.getLogger(MySqlConecter.class.getName());
        } catch (Exception ex) {
            Logger.getLogger(MySqlConecter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public String getConnectString() {

        StringBuilder sb = new StringBuilder("");

        sb.append(getUri());
        sb.append(getHost());
        if ( getPort() != null ) {
            sb.append(":");
            sb.append(getPort());
        }
        sb.append("/");
        sb.append(getDatabase());
        sb.append("?");
        sb.append("user=");
        sb.append(getUser());
        sb.append("&password=");
        sb.append(getPassword());
        sb.append(getOtherSetting());

        log.log(Level.INFO, "Connection String {0}", sb.toString());

        return sb.toString();
    }

    @Override
    public void valid()
            throws SQLException {

        if ( getHost() == null ) {
            throw new SQLException("Host cannot be blank");
        }

        if ( getUser() == null ) {
            throw new SQLException("User name cannot be blank");
        }

        if ( getPassword() == null ) {
            throw new SQLException("Password cannot be blank");
        }
        if ( getDatabase() == null ) {
            throw new SQLException("Database name cannot be blank");
        }
    }

}
