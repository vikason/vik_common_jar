package vik.sql;

import java.sql.SQLException;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public abstract class DbConnecter {

    private String driverName = "";
    private String uri = "";
    private String user = "";
    private String password = "";
    private String port = "";
    private String host = "";
    private String database = "";
    private String otherSetting = "";
    private boolean create = false;

    /**
     *
     */
    protected final String MySQL_DRIVER = "com.mysql.jdbc.Driver";

    /**
     *
     */
    protected final String MySQL_URI = "jdbc:mysql://";

    /**
     *
     */
    protected final String ORACLE_DRIVER = "";

    /**
     *
     */
    protected final String EMBEDED_DERBY_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";

    /**
     *
     */
    protected final String EMBEDED_DERBY_URI = "jdbc:derby:database";

    /**
     *
     * @return
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     *
     * @param driverName
     */
    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    /**
     *
     * @return
     */
    public String getUri() {
        return uri;
    }

    /**
     *
     * @param uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     *
     * @return
     */
    public String getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    public String getPort() {
        return port;
    }

    /**
     *
     * @param port
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     *
     * @return
     */
    public String getHost() {
        return host;
    }

    /**
     *
     * @param host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     *
     * @return
     */
    public String getDatabase() {
        return database;
    }

    /**
     *
     * @param database
     */
    public void setDatabase(String database) {
        this.database = database;
    }

    /**
     *
     * @return
     */
    public String getOtherSetting() {
        return otherSetting;
    }

    /**
     *
     * @param otherSetting
     */
    public void setOtherSetting(String otherSetting) {
        this.otherSetting = otherSetting;
    }

    public boolean isCreate() {
        return create;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    /**
     *
     * @return
     */
    public abstract String getConnectString();

    /**
     *
     * @throws SQLException
     */
    public abstract void valid() throws SQLException;
}
