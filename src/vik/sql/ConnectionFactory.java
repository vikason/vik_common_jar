package vik.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vik.log.TestLogger;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class ConnectionFactory {

    private DbConnecter connect;
    private Logger log = Logger.getLogger(ConnectionFactory.class.getName());
    private String connectionString;
    private static Connection con = null;

    /**
     * Connection string used to connect to DB.
     * <p>
     * @return
     */
    public String getConnectionString() {
        return connectionString;
    }

    /**
     * Constructor for this class.
     * <p>
     * @param connect: need to initialize connection settings.
     */
    public ConnectionFactory(DbConnecter connect) {
        this.connect = connect;
        try {
            log = TestLogger.getLogger(DbUtility.class.getName());
        } catch(Exception ex) {
            Logger.getLogger(DbUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Get DB Connection
     * <p>
     * @return <code>DbConnecterion</code>
     * <p>
     * @throws ClassNotFoundException <p>
     * @throws SQLException           <p>
     * This is a thread safe method and singleton method, only one instance of
     * connection can be created.
     */
    public Connection getConnection() throws ClassNotFoundException, SQLException {

        /**
         * singleton object so that only one instance should create
         */
        synchronized(ConnectionFactory.class) {
            /**
             * synchronized block so that every thread must wait until last
             * thread complete the action.
             */
            if( con == null ) {
                /**
                 * get connection object
                 */
                getCon();
            }
        }

        return con;
    }

    /**
     * Initialize con object.
     * <p>
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private void getCon() throws ClassNotFoundException, SQLException {

        try {
            /**
             * load db driver.
             */
            log.log(Level.FINE, "Loading database driver...{0}", connect.getDriverName());
            Class.forName(connect.getDriverName());

            /**
             * do validation before connection
             */
            log.fine("checking connection paratmeres ...");
            connect.valid();

            connectionString = connect.getConnectString();
            /**
             * return db connection.
             */
            log.fine("retriving database connection ...");
            con = DriverManager.getConnection(connectionString);

        } catch(ClassNotFoundException | SQLException e) {
            log.severe("Connection String");
            log.severe(getConnectionString());
            log.log(Level.SEVERE, e.getMessage());
            throw e;
        }

    }

}
