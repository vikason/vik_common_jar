package vik.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vik.log.TestLogger;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public abstract class DbUtility {

    private Connection con;
    private Logger log = Logger.getLogger(DbUtility.class.getName());

    /**
     *
     */
    public PreparedStatement ps;

    /**
     *
     */
    public ResultSet rs;

    /**
     *
     */
    public DbUtility() {
        try {
            log = TestLogger.getLogger(DbUtility.class.getName());
        } catch(Exception ex) {
            Logger.getLogger(DbUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param sql
     * <p>
     * @return
     * <p>
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public vik.sql.PreparedStatement getPreparedStatement(String sql)
            throws ClassNotFoundException, SQLException {
        if( con == null ) {
            ConnectionFactory cf = new ConnectionFactory(getConnect());
            con = cf.getConnection();
            if( con != null ) {
                log.fine("Connected to DB successfully");
            } else {
                log.severe("Connection object is null");
                log.severe("Connection String: ");
                log.severe(cf.getConnectionString());
            }
        }
        PreparedStatement ps = new PreparedStatement(con, sql);

        return ps;
    }

    /**
     * abstract factory method need for connections.
     * <p>
     * @return connection object returned from subclass
     */
    public abstract DbConnecter getConnect();

}
