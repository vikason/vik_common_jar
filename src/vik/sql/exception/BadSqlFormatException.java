package vik.sql.exception;

/**
 *
 * @author VIKAS
 * @company Subhash Technologies, LTD. Nagda
 */
public class BadSqlFormatException extends Exception {

    private String sql;
    private int parameterCount;
    private int parameterPassed;
    private final String msg
            = "Parameters count mis-Match";

    /**
     *
     * @param sql
     * @param parameterCount
     * @param parameterPassed
     */
    public BadSqlFormatException(String sql,
            int parameterCount,
            int parameterPassed) {
        this.sql = sql;
        this.parameterCount = parameterCount;
        this.parameterPassed = parameterPassed;

        System.out.println("Exception found while processing preparedStatement");
        System.out.println(msg);
        System.out.println("DB SQL: " + sql);
        System.out.println("Parameter count in sql: " + parameterCount);
        System.out.println("Total Parameter Passed: " + parameterPassed);
    }

    /**
     * Creates a new instance of <code>BadSqlFormatException</code> without
     * detail message.
     */
    public BadSqlFormatException() {
    }

    /**
     * Constructs an instance of <code>BadSqlFormatException</code> with the
     * specified detail message.
     * <p>
     * @param msg the detail message.
     */
    public BadSqlFormatException(String msg) {
        super(msg);

    }
}
