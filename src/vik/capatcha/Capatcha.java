/**
 * Class to generate Capatcha Text image.
 */
package vik.capatcha;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author Vikas Verma
 */
public class Capatcha {

    /**
     * Default constructor
     */
    public Capatcha() {
    }


    /**
     * <p>
     * This method creates a picture file containing the text string passed as
     * parameter to this method.<p>
     *
     * @param text-Text string need to convert in picture file.
     * @param size-Font size of the text.
     * @param fontFilePath-Absolute font file path for the text string.
     * @return <code>BufferedImage</code> object.
     *
     * @throws FontFormatException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static BufferedImage getBufferedImage(String text, float size, String fontFilePath)
            throws FontFormatException, FileNotFoundException, IOException {

        //----------------------- variable declaration.
        Font font = null;
        BufferedImage bufferedImage = null;
        Graphics2D graphics2D = null;
        FontRenderContext fontRenderContext = null;
        Rectangle2D rectangle2D = null;
        int width = 0;
        int height = 0;
        Color backgroundColor;
        Color fontColor = Color.white;

        backgroundColor = Color.black;
        fontColor = Color.white;

        //----------------------- set font for the text image.
        font = Font.createFont(Font.TRUETYPE_FONT,
                new FileInputStream(fontFilePath));

        font = font.deriveFont(size);

        //----------------------- logic to estimaated size of text start.
        //----------------------- craete buffer image.
        bufferedImage =
                new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);

        //----------------------- set graphics.
        graphics2D = bufferedImage.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        fontRenderContext = graphics2D.getFontRenderContext();
        rectangle2D = font.getStringBounds(text, fontRenderContext);

        //----------------------- calculate the size of the text.
        width = (int) rectangle2D.getWidth();
        height = (int) rectangle2D.getHeight();

        //----------------------- logic to estimaated size of text end.
        //----------------------- create image of calculated text size.
        bufferedImage = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);

        //----------------------- set graphics.
        graphics2D = bufferedImage.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setFont(font);

        //----------------------- set graphics.
        graphics2D.setColor(backgroundColor);
        graphics2D.fillRect(0, 0, width, height);
        graphics2D.setColor(fontColor);
        graphics2D.drawString(text, 0, (int) -rectangle2D.getY());

        //----------------------- returned buffered image.
        return bufferedImage;
    } // getBufferedImage();
}
/* end of class */